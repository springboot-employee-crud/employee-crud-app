# README #

### What is this repository for? ###

This repository contains Employee CRUD application which is developed by following Domain Driven architecture. It also contains development of reading a JSON file and get data for a person by giving the phone number. 

* Technologies used - 
	- Spring Boot
	- Mongo DB
	- Log4j
	- Jackson databind
	- Junit
	- Mockito

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner