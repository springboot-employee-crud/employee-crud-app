package com.emp.infrastructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Configuration
@PropertySource("classpath:application.properties")
public class MongoDbConfiguration {

	@Value("${spring.data.mongodb.uri}")
	private String url;

	@Bean
	public MongoClient mongoClient() {
		ConnectionString connectionString = new ConnectionString(url);
		MongoClientSettings clientSettings = MongoClientSettings.builder().applyConnectionString(connectionString)
				.build();

		return MongoClients.create(clientSettings);
	}
}
