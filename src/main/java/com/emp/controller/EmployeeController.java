package com.emp.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.emp.controller.common.RequestMappings;
import com.emp.domain.AllEmployeeResponse;
import com.emp.domain.AllPersonResponse;
import com.emp.domain.CreateEmployeeRequest;
import com.emp.domain.CreateEmployeeResponse;
import com.emp.domain.CreatePersonRequest;
import com.emp.domain.DeleteEmployeeResponse;
import com.emp.domain.EmployeeResponse;
import com.emp.domain.PersonResponse;
import com.emp.domain.UpdateEmployeeRequest;
import com.emp.domain.common.BadRequestException;
import com.emp.domain.util.EmployeeValidator;
import com.emp.service.EmployeeService;

@RestController
public class EmployeeController {

	private final String EMPLOYEE_ENDPOINTS_RUNNING = "Employee CRUD endpoints are running";

	final static Logger logger = Logger.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private EmployeeValidator employeeValidator;

	/*
	 * Home
	 */
	@RequestMapping(value = RequestMappings.CONTEXT_PATH, method = RequestMethod.GET)
	public String home() {
		return EMPLOYEE_ENDPOINTS_RUNNING;
	}

	/*
	 * Create employee
	 */
	@RequestMapping(value = RequestMappings.CREATE_EMPLOYEE, method = RequestMethod.POST)
	public @ResponseBody CreateEmployeeResponse addEmployee(@RequestHeader("api-key") String apiKey,
			@RequestBody CreateEmployeeRequest request) {

		logger.info("Inside create new employee endpoint");

		try {

			employeeValidator.isValidApiKey(apiKey);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new BadRequestException("API key is invalid");
		}

		return employeeService.createEmployee(request);

	}

	/*
	 * Get all employees
	 */
	@RequestMapping(value = RequestMappings.GET_ALL_EMPLOYEES, method = RequestMethod.GET)
	public @ResponseBody AllEmployeeResponse getAllEmployees(@RequestHeader("api-key") String apiKey) {

		logger.info("Inside get all employees endpoint");

		try {

			employeeValidator.isValidApiKey(apiKey);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new BadRequestException("API key is invalid");
		}

		return employeeService.getAllEmployees();
	}

	/*
	 * Delete employee by id
	 */
	@RequestMapping(value = RequestMappings.DELETE_EMPLOYEE_BY_ID, method = RequestMethod.DELETE)
	public @ResponseBody DeleteEmployeeResponse deleteEmployeeResponse(@RequestHeader("api-key") String apiKey,
			@PathVariable(value = "emp_id", required = true) String employeeId) {

		logger.info("Inside delete employee by id endpoint");

		try {

			employeeValidator.isValidApiKey(apiKey);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new BadRequestException("API key is invalid");
		}

		return employeeService.deleteEmployeeById(employeeId);
	}

	/*
	 * Update employee by id
	 */
	@RequestMapping(value = RequestMappings.UPDATE_EMPLOYEE_BY_ID, method = RequestMethod.PUT)
	public @ResponseBody EmployeeResponse updateEmployeeById(@RequestHeader("api-key") String apiKey,
			@RequestBody UpdateEmployeeRequest request,
			@PathVariable(value = "emp_id", required = true) String employeeId) {

		logger.info("Inside update employee by id endpoint");

		try {

			employeeValidator.isValidApiKey(apiKey);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new BadRequestException("API key is invalid");
		}

		return employeeService.updateEmployee(employeeId, request);
	}

	/*
	 * Get employee by id
	 */
	@RequestMapping(value = RequestMappings.GET_EMPLOYEE_BY_ID, method = RequestMethod.GET)
	public @ResponseBody EmployeeResponse getEmployeeById(@RequestHeader("api-key") String apiKey,
			@PathVariable(value = "emp_id", required = true) String employeeId) {

		logger.info("Inside get employee by id endpoint");

		try {

			employeeValidator.isValidApiKey(apiKey);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new BadRequestException("API key is invalid");
		}

		return employeeService.getEmployeeById(employeeId);
	}

	/*
	 * Get person
	 */
	@RequestMapping(value = RequestMappings.GET_ALL_PERSONS, method = RequestMethod.GET)
	public @ResponseBody AllPersonResponse getAllPersons() {

		logger.info("Inside get all persons endpoint");

		return employeeService.getAllPersons();
	}

	/*
	 * Get person by phone number
	 */
	@RequestMapping(value = RequestMappings.GET_PERSON_BY_PHONE, method = RequestMethod.GET)
	public @ResponseBody PersonResponse getPersonByPhone(@RequestBody CreatePersonRequest request) {

		logger.info("Inside get person by phone number endpoint");

		return employeeService.getPersonDataByPhone(request);
	}
}
