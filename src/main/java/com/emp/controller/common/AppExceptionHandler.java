package com.emp.controller.common;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.emp.domain.common.ApiError;
import com.emp.domain.common.BadRequestException;
import com.emp.domain.common.NotFoundException;

@ControllerAdvice
public class AppExceptionHandler {

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<?> handleResourceNotFoundException(NotFoundException notFoundException, WebRequest request) {

		String errorMessage = notFoundException.getMessage();

		if (errorMessage == null) {
			errorMessage = notFoundException.toString();
		}
		ApiError apiError = new ApiError(new Date(), errorMessage, request.getDescription(false));

		return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<?> handleBadRequestException(BadRequestException badRequestException, WebRequest request) {

		String errorMessage = badRequestException.getMessage();

		if (errorMessage == null) {
			errorMessage = badRequestException.toString();
		}
		ApiError apiError = new ApiError(new Date(), errorMessage, request.getDescription(false));

		return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NullPointerException.class)
	public ResponseEntity<?> handleNullPointerException(NullPointerException nullPointerException, WebRequest request) {

		String errorMessage = nullPointerException.getMessage();

		if (errorMessage == null) {
			errorMessage = nullPointerException.toString();
		}
		ApiError apiError = new ApiError(new Date(), errorMessage, request.getDescription(false));

		return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
	}
}
