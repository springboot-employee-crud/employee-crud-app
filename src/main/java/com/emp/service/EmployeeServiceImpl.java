package com.emp.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.emp.domain.AllEmployeeResponse;
import com.emp.domain.AllPersonResponse;
import com.emp.domain.CreateEmployeeRequest;
import com.emp.domain.CreateEmployeeResponse;
import com.emp.domain.CreatePersonRequest;
import com.emp.domain.DeleteEmployeeResponse;
import com.emp.domain.Employee;
import com.emp.domain.EmployeeResponse;
import com.emp.domain.Person;
import com.emp.domain.PersonResponse;
import com.emp.domain.UpdateEmployeeRequest;
import com.emp.domain.common.BadRequestException;
import com.emp.domain.common.NotFoundException;
import com.emp.domain.transformer.EmployeeTransformer;
import com.emp.domain.util.EmployeeValidator;
import com.emp.repository.EmployeeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	final static Logger logger = Logger.getLogger(EmployeeServiceImpl.class);

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private EmployeeValidator validator;

	@Value("classpath:json/address.json")
	Resource resourceFile;

	@Override
	public AllEmployeeResponse getAllEmployees() {
		logger.info("Inside get all employees service");

		List<Employee> employees = null;
		AllEmployeeResponse allEmployeeResponse = null;

		try {

			employees = employeeRepository.findAll();

			if (employees.isEmpty()) {
				throw new NotFoundException("Employees not found");
			}
			allEmployeeResponse = EmployeeTransformer.employeesToAllEmployeeResponse(employees);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new NotFoundException("Error while loading employees");
		}

		logger.info("Successfully retrieved all employees details");
		return allEmployeeResponse;
	}

	@Override
	public CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest) {
		logger.info("Inside create employee service");

		Assert.hasText(createEmployeeRequest.getName(), "Employee name is required");
		Assert.hasText(createEmployeeRequest.getAddress(), "Employee address is required");
		Assert.notNull(createEmployeeRequest.getSalary(), "Employee salary is required");

		validator.isValidEmail(createEmployeeRequest.getEmail());
		validator.isValidPhone(createEmployeeRequest.getPhone());

		Employee employee = null;
		CreateEmployeeResponse employeeResponse = null;

		try {

			employee = EmployeeTransformer.transformEmployeeRequestToDto(createEmployeeRequest);

			employee = employeeRepository.save(employee);
			employeeResponse = EmployeeTransformer.transformEmployeeDtoToResponse(employee);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new BadRequestException("Error while creating employee");

		}

		logger.info("Successfully created the employee");
		return employeeResponse;
	}

	@Override
	public DeleteEmployeeResponse deleteEmployeeById(String employeeId) {
		logger.info("Inside delete employee by id service");

		DeleteEmployeeResponse deleteEmployeeResponse = null;
		Optional<Employee> optionalEmployee = null;

		try {
			if (employeeId != null && !employeeId.isEmpty()) {

				optionalEmployee = employeeRepository.findById(employeeId);

				if (!optionalEmployee.isPresent()) {
					throw new NotFoundException("Employee not found for the given id");
				}

				employeeRepository.deleteById(employeeId);
			} else {
				throw new NotFoundException("Employee Id cannot be null");
			}

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new BadRequestException("Error found while deleting employee");
		}

		logger.info("Successfully deleted the employee");
		return deleteEmployeeResponse;
	}

	@Override
	public EmployeeResponse updateEmployee(String employeeId, UpdateEmployeeRequest request) {
		logger.info("Inside update employee by id service");

		Employee existingEmployee = null;
		EmployeeResponse employeeResponse = null;

		try {

			if (!employeeRepository.findById(employeeId).isPresent()) {

				throw new NotFoundException("Employee not found for the given id");
			}

			existingEmployee = employeeRepository.findById(employeeId).get();

			if (StringUtils.hasText(request.getName())) {
				existingEmployee.setName(request.getName());
			}
			if (StringUtils.hasText(request.getAddress())) {
				existingEmployee.setAddress(request.getAddress());
			}
			if (request.getSalary() != null) {
				existingEmployee.setSalary(request.getSalary());
			}
//			if (StringUtils.hasText(request.getPhone())) {
//
//				if (validator.isValidPhone(request.getPhone())) {
//					existingEmployee.setPhone(request.getPhone());
//				}
//			}
//			if (StringUtils.hasText(request.getEmail())) {
//
//				if (validator.isValidEmail(request.getEmail())) {
//					existingEmployee.setEmail(request.getEmail());
//				}
//			}

			validator.isValidPhone(request.getPhone());
			existingEmployee.setPhone(request.getPhone());

			validator.isValidEmail(request.getEmail());
			existingEmployee.setEmail(request.getEmail());

			employeeRepository.save(existingEmployee);
			employeeResponse = EmployeeTransformer.employeeToEmployeeResponse(existingEmployee);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new NotFoundException("Error found when updating the employee.");
		}

		logger.info("Successfully updated the employee");
		return employeeResponse;
	}

	@Override
	public EmployeeResponse getEmployeeById(String employeeId) {
		logger.info("Inside get employee by id service");

		Optional<Employee> optionalEmployee = null;
		Employee employee = null;
		EmployeeResponse employeeResponse = null;

		try {
			optionalEmployee = employeeRepository.findById(employeeId);

			if (!optionalEmployee.isPresent()) {
				throw new NotFoundException("Employee not found for the given id");

			}
			employee = optionalEmployee.get();

			employeeResponse = EmployeeTransformer.employeeToEmployeeResponse(employee);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new BadRequestException("Error found when retrieving the employee.");
		}

		logger.info("Successfully retrieved the employee by id");
		return employeeResponse;
	}

	@Override
	public AllPersonResponse getAllPersons() {
		logger.info("Inside get person service");

		ObjectMapper objectMapper = new ObjectMapper();

		AllPersonResponse allPersonResponse = null;

		try {

			if (!resourceFile.exists()) {
				throw new NotFoundException("Resource file does not exists");
			}

			InputStream inputStream = resourceFile.getInputStream();

			// Read the json file
			List<Person> personList = objectMapper.readValue(inputStream, new TypeReference<ArrayList<Person>>() {
			});

			allPersonResponse = EmployeeTransformer.personsToAllPersons(personList);

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new BadRequestException("Error while loading json data from file");
		}

		logger.info("Successfully retrieved json data");
		return allPersonResponse;

	}

	@Override
	public PersonResponse getPersonDataByPhone(CreatePersonRequest createPersonRequest) {
		logger.info("Inside get person by phone number service");

		ObjectMapper objectMapper = new ObjectMapper();

		PersonResponse personResponse = new PersonResponse();

		try {

			// Check existing of the json file
			if (!resourceFile.exists()) {
				throw new NotFoundException("Resource file does not exists");
			}

			InputStream inputStream = resourceFile.getInputStream();

			// Read the json file
			List<Person> personList = objectMapper.readValue(inputStream, new TypeReference<ArrayList<Person>>() {
			});

			if (!createPersonRequest.getPhoneNumber().isEmpty() || !(createPersonRequest.getPhoneNumber() == null)) {

				if (validator.isValidPhoneNumber(createPersonRequest.getPhoneNumber())) {

					if (personList.size() > 0) {
						for (Person person : personList) {

							if (person.getPhoneNumber().equals(createPersonRequest.getPhoneNumber())) {
								personResponse.setPerson(person);
							}
						}
					}
				}
			}

		} catch (NullPointerException exception) {
			logger.error("Error found : " + exception.getMessage().toString());
			throw new com.emp.domain.common.NullPointerException("Phone number cannot be null");

		} catch (Exception e) {
			logger.error("Error found : " + e.getMessage().toString());
			throw new BadRequestException("Error found while loading person for given phone number");
		}

		logger.info("Successfully retrieved the person by phone number");
		return personResponse;
	}

}
