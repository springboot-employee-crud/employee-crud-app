package com.emp.service;

import com.emp.domain.AllEmployeeResponse;
import com.emp.domain.AllPersonResponse;
import com.emp.domain.CreateEmployeeRequest;
import com.emp.domain.CreateEmployeeResponse;
import com.emp.domain.CreatePersonRequest;
import com.emp.domain.DeleteEmployeeResponse;
import com.emp.domain.EmployeeResponse;
import com.emp.domain.PersonResponse;
import com.emp.domain.UpdateEmployeeRequest;

public interface EmployeeService {

	AllEmployeeResponse getAllEmployees();

	CreateEmployeeResponse createEmployee(CreateEmployeeRequest createEmployeeRequest);

	DeleteEmployeeResponse deleteEmployeeById(String employeeId);

	EmployeeResponse updateEmployee(String employeeId, UpdateEmployeeRequest request);

	EmployeeResponse getEmployeeById(String employeeId);

	AllPersonResponse getAllPersons();

	PersonResponse getPersonDataByPhone(CreatePersonRequest createPersonRequest);
}
