package com.emp.domain.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.emp.domain.common.BadRequestException;

@Component
public class EmployeeValidator {

	final static Logger logger = Logger.getLogger(EmployeeValidator.class);

	@Value("${regex.pattern.email}")
	private String emailRegex;

	@Value("${regex.pattern.phone}")
	private String phoneRegex;

	@Value("${regex.json.phone}")
	private String jsonPhone;

	@Value("${api.key}")
	private String key;

	public boolean isValidPhone(String phone) {
		Assert.hasText(phone, "Employee phone is required");

		Pattern pattern_phone = Pattern.compile(phoneRegex);
		Matcher matcher = pattern_phone.matcher(phone);

		try {
			if (!matcher.matches()) {
				throw new BadRequestException("Invalid phone");
			}

		} catch (Exception e) {
			logger.error("Validation error found : " + e.getMessage());
			throw new BadRequestException("Phone number is invalid");
		}

		return matcher.matches();
	}

	public boolean isValidEmail(String email) {
		Assert.hasText(email, "Employee email is required");

		Pattern pattern_email = Pattern.compile(emailRegex);
		Matcher matcher = pattern_email.matcher(email);

		try {
			if (!matcher.matches()) {
				throw new BadRequestException("Invalid email");
			}

		} catch (Exception e) {
			logger.error("Validation error found : " + e.getMessage());
			throw new BadRequestException("Email is invalid");
		}

		return matcher.matches();
	}

	public boolean isValidPhoneNumber(String phone) {
		Pattern pattern = Pattern.compile(jsonPhone);
		Matcher matcher = pattern.matcher(phone);

		try {
			if (!matcher.matches()) {
				throw new BadRequestException("Invalid phone number");
			}

		} catch (Exception e) {
			logger.error("Validation error found : " + e.getMessage());
			throw new BadRequestException("Phone number is invalid");
		}
		return matcher.matches();
	}

	public boolean isValidApiKey(String apiKey) {

		try {
			if (!apiKey.equals(key)) {
				throw new BadRequestException("Invalid API key");
			}

		} catch (Exception e) {
			logger.error("Validation error found : " + e.getMessage());
			throw new BadRequestException("API key is invalid");
		}

		return true;
	}
}
