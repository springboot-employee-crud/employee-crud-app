package com.emp.domain.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.emp.domain.AllEmployeeResponse;
import com.emp.domain.AllPersonResponse;
import com.emp.domain.CreateEmployeeRequest;
import com.emp.domain.CreateEmployeeResponse;
import com.emp.domain.Employee;
import com.emp.domain.EmployeeResponse;
import com.emp.domain.Person;
import com.emp.domain.PersonResponse;

public class EmployeeTransformer {

	public static Employee transformEmployeeRequestToDto(CreateEmployeeRequest createEmployeeRequest) {

		Employee employee = new Employee();

		employee.setEmpId(UUID.randomUUID().toString());
		employee.setName(createEmployeeRequest.getName());
		employee.setAddress(createEmployeeRequest.getAddress());
		employee.setSalary(createEmployeeRequest.getSalary());
		employee.setPhone(createEmployeeRequest.getPhone());
		employee.setEmail(createEmployeeRequest.getEmail());

		return employee;
	}

	public static CreateEmployeeResponse transformEmployeeDtoToResponse(Employee employee) {

		CreateEmployeeResponse employeeResponse = new CreateEmployeeResponse();

		employeeResponse.setName(employee.getName());
		employeeResponse.setAddress(employee.getAddress());
		employeeResponse.setSalary(employee.getSalary());
		employeeResponse.setPhone(employee.getPhone());
		employeeResponse.setEmail(employee.getEmail());

		return employeeResponse;
	}

	public static EmployeeResponse employeeToEmployeeResponse(Employee employee) {

		EmployeeResponse employeeResponse = new EmployeeResponse();

		employeeResponse.setName(employee.getName());
		employeeResponse.setAddress(employee.getAddress());
		employeeResponse.setSalary(employee.getSalary());
		employeeResponse.setPhone(employee.getPhone());
		employeeResponse.setEmail(employee.getEmail());

		return employeeResponse;
	}

	public static AllEmployeeResponse employeesToAllEmployeeResponse(List<Employee> employees) {

		AllEmployeeResponse allEmployeeResponse = new AllEmployeeResponse();
		List<EmployeeResponse> employeeResponses = new ArrayList<EmployeeResponse>();

		for (Employee employee : employees) {
			EmployeeResponse response = new EmployeeResponse();
			response.setName(employee.getName());
			response.setAddress(employee.getAddress());
			response.setSalary(employee.getSalary());
			response.setPhone(employee.getPhone());
			response.setEmail(employee.getEmail());
			employeeResponses.add(response);
		}

		allEmployeeResponse.setEmployees(employeeResponses);
		return allEmployeeResponse;
	}

	public static AllPersonResponse personsToAllPersons(List<Person> persons) {

		AllPersonResponse allPersonResponse = new AllPersonResponse();
		List<PersonResponse> personResponses = new ArrayList<PersonResponse>();

		for (Person person : persons) {
			PersonResponse response = new PersonResponse();
			response.setPerson(person);
			personResponses.add(response);
		}

		allPersonResponse.setPersonResponses(personResponses);
		return allPersonResponse;
	}
}
