package com.emp.domain.common;

public class NullPointerException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NullPointerException(String statusMessage) {
		super(statusMessage);
	}
}
