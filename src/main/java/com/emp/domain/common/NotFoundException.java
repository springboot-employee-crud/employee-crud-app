package com.emp.domain.common;

public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundException(String statusMessage) {
		super(statusMessage);
	}
}
