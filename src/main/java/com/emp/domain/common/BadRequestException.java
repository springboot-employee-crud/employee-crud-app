package com.emp.domain.common;

public class BadRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadRequestException(String statusMessage) {
		super(statusMessage);
	}
}
