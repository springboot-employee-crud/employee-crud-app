package com.emp.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {

	@JsonProperty(value = "phone_number")
	private String phoneNumber;

	@JsonProperty(value = "first_name")
	private String firstName;

	@JsonProperty(value = "last_name")
	private String lastName;

	@JsonProperty(value = "last_4_digit_cc")
	private String lastDigitCC;

	@JsonProperty(value = "gender")
	private String gender;

	@JsonProperty(value = "email")
	private String email;

	@JsonProperty(value = "address")
	private Address address;

	@JsonProperty(value = "credit_card")
	private String creditCard;

	@JsonProperty(value = "order_number")
	private String orderNumber;

	@JsonProperty(value = "id")
	private long id;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastDigitCC() {
		return lastDigitCC;
	}

	public void setLastDigitCC(String lastDigitCC) {
		this.lastDigitCC = lastDigitCC;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Person [phoneNumber=" + phoneNumber + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", lastDigitCC=" + lastDigitCC + ", gender=" + gender + ", email=" + email + ", address=" + address
				+ ", creditCard=" + creditCard + ", orderNumber=" + orderNumber + ", id=" + id + "]";
	}

}
