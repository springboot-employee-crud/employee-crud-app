package com.emp.domain;

import java.util.ArrayList;
import java.util.List;

public class AllPersonResponse {

	private List<PersonResponse> personResponses;

	public AllPersonResponse() {
	}

	public List<PersonResponse> getPersons() {
		if (personResponses == null) {
			personResponses = new ArrayList<PersonResponse>();
		}
		return personResponses;
	}

	public void setPersonResponses(List<PersonResponse> personResponses) {
		this.personResponses = personResponses;
	}
}
