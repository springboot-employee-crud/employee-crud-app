package com.emp.controller.util;

import java.util.ArrayList;
import java.util.List;

import com.emp.domain.AllEmployeeResponse;
import com.emp.domain.CreateEmployeeRequest;
import com.emp.domain.CreateEmployeeResponse;
import com.emp.domain.CreatePersonRequest;
import com.emp.domain.Employee;
import com.emp.domain.EmployeeResponse;
import com.emp.domain.transformer.EmployeeTransformer;

public class MockDtoUtil {

	public CreateEmployeeRequest getEmployeeRequest() {
		final CreateEmployeeRequest request = new CreateEmployeeRequest();

		request.setName("Dasuni");
		request.setAddress("Court Road, Colombo");
		request.setSalary(250700.00);
		request.setPhone("+91 3423 546443");
		request.setEmail("dasuni.a@aeturnum.com");

		return request;
	}

	public CreateEmployeeResponse getEmployeeResponse(CreateEmployeeRequest request) {

		Employee employee = EmployeeTransformer.transformEmployeeRequestToDto(request);

		CreateEmployeeResponse employeeResponse = EmployeeTransformer.transformEmployeeDtoToResponse(employee);

		return employeeResponse;
	}

	public EmployeeResponse getEmployeeResponse() {
		Employee employee = new Employee();

		employee.setName("Test");
		employee.setAddress("Sri Lanka");
		employee.setSalary(90000.00);
		employee.setPhone("+94 78 933-8000");
		employee.setEmail("test@hotmail.com");

		EmployeeResponse response = EmployeeTransformer.employeeToEmployeeResponse(employee);
		return response;

	}

	public AllEmployeeResponse getAllEmployees() {

		Employee employee1 = new Employee();
		employee1.setName("Dasuni");
		employee1.setAddress("Colombo 6");
		employee1.setSalary(350000.00);
		employee1.setPhone("+94 78 933-8000");
		employee1.setEmail("dasuni.a@aeturnum.com");

		Employee employee2 = new Employee();
		employee2.setName("Dinushi");
		employee2.setAddress("Colombo 10");
		employee2.setSalary(67000.00);
		employee2.setPhone("+94 71 935-8780");
		employee2.setEmail("dinu123@gmail.com");

		List<Employee> employees = new ArrayList<Employee>();
		employees.add(employee1);
		employees.add(employee2);

		AllEmployeeResponse allEmployeeResponse = EmployeeTransformer.employeesToAllEmployeeResponse(employees);

		return allEmployeeResponse;
	}

	public CreatePersonRequest getPersonRequest() {
		final CreatePersonRequest request = new CreatePersonRequest();

		request.setPhoneNumber("156-698-4867");

		return request;
	}

//	public PersonResponse getPersonResponse(CreatePersonRequest request) {
//		
//		Person person =  EmployeeTransformer.personsToAllPersons(request);
//	}
}
