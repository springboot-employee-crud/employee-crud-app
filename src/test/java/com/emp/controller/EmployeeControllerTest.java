package com.emp.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.emp.controller.common.RequestMappings;
import com.emp.controller.util.MockDtoUtil;
import com.emp.domain.AllEmployeeResponse;
import com.emp.domain.CreateEmployeeRequest;
import com.emp.domain.CreateEmployeeResponse;
import com.emp.domain.DeleteEmployeeResponse;
import com.emp.domain.EmployeeResponse;
import com.emp.domain.UpdateEmployeeRequest;
import com.emp.domain.util.EmployeeValidator;
import com.emp.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest
public class EmployeeControllerTest {

	private final String API_KEY = "api-key";

	private MockMvc mockMvc;

	private MvcResult mvcResult;

	MockDtoUtil dtoUtil = new MockDtoUtil();

	@Mock
	EmployeeValidator validator;

	@Mock
	private EmployeeService employeeService;

	@InjectMocks
	private EmployeeController employeeController;

	private ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(this.employeeController).build();
	}

	@Test
	public void createEmployeeTest() throws Exception {

		CreateEmployeeResponse response = dtoUtil.getEmployeeResponse(dtoUtil.getEmployeeRequest());

		Mockito.when(employeeService.createEmployee(Matchers.isA(CreateEmployeeRequest.class))).thenReturn(response);

		mvcResult = mockMvc.perform(
				post(RequestMappings.CREATE_EMPLOYEE).headers(setHeaders()).contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(objectMapper.writeValueAsString(dtoUtil.getEmployeeRequest()))
						.accept(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk()).andDo(print()).andReturn();

		String jsonResponse = mvcResult.getResponse().getContentAsString();
		assertEquals(jsonResponse, objectMapper.writeValueAsString(response));

	}

	@Test
	public void getAllEmployeesTest() throws Exception {

		Mockito.when(employeeService.getAllEmployees()).thenReturn(dtoUtil.getAllEmployees());

		mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(RequestMappings.GET_ALL_EMPLOYEES).headers(setHeaders())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andDo(print()).andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

		String jsonResponse = mvcResult.getResponse().getContentAsString();
		AllEmployeeResponse response = objectMapper.readValue(jsonResponse, AllEmployeeResponse.class);
		assertThat(response.getEmployees().size()).isEqualTo(2);
	}

	@Test
	public void updateEmployeeTest() throws Exception {

		String empId = "40e0e1ce-d763-44b4-a79c-b513c343cd6e";

		EmployeeResponse employee = employeeService.getEmployeeById(empId);

		UpdateEmployeeRequest employeeRequest = new UpdateEmployeeRequest();
		employeeRequest.setName("Dinushi");
		employeeRequest.setAddress("Colombo 9");
		employeeRequest.setSalary(450890.00);
		employeeRequest.setPhone("+91 3423 546443");
		employeeRequest.setEmail("dinu123@gmail.com");

		Mockito.when(employeeService.updateEmployee(empId, employeeRequest)).thenReturn(employee);

		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put(RequestMappings.UPDATE_EMPLOYEE_BY_ID, empId)
				.headers(setHeaders()).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(employee));

		mvcResult = (MvcResult) mockMvc.perform(builder).andExpect(status().isCreated())
				.andExpect(jsonPath("$.empId", is(empId)))
				.andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(employee)));
	}

	@Test
	public void deleteEmployeeTest() throws Exception {

		String empId = "d729cfdc-5956-4be2-b995-1d21251f8d74";

		DeleteEmployeeResponse response = new DeleteEmployeeResponse();

		Mockito.when(employeeService.deleteEmployeeById(empId)).thenReturn(response);

		this.mockMvc.perform(delete(RequestMappings.DELETE_EMPLOYEE_BY_ID, empId).headers(setHeaders())
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted());

		verify(employeeService, times(1)).deleteEmployeeById(empId);
	}

	@Test
	public void getEmployeeByIdTest() throws Exception {

		String empId = "8d6215b5-ba1c-4b86-bc65-27478957aa04";

		EmployeeResponse employeeResponse = dtoUtil.getEmployeeResponse();

		Mockito.when(employeeService.getEmployeeById(empId)).thenReturn(employeeResponse);

		mvcResult = mockMvc.perform(get(RequestMappings.GET_EMPLOYEE_BY_ID, empId).headers(setHeaders())
				.contentType(MediaType.APPLICATION_JSON))
//						.content(objectMapper.writeValueAsString(dtoUtil.getEmployeeResponse())))
				.andExpect(status().isOk()).andExpect(status().isOk()).andDo(print()).andReturn();

		String responseString = mvcResult.getResponse().getContentAsString();
		assertEquals(responseString, objectMapper.writeValueAsString(employeeResponse));
	}

	@Test
	public void deleteEmployeeIdNotFoundTest() throws Exception {

		Mockito.when(employeeService.deleteEmployeeById("d729cfdc-5956-4be2-b995-1d21251f8d745")).thenReturn(null);

		mockMvc.perform(delete(RequestMappings.DELETE_EMPLOYEE_BY_ID, "d729cfdc-5956-4be2-b995-1d21251f8d745")
				.headers(setHeaders()).accept(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	private HttpHeaders setHeaders() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set(API_KEY, "empApi@#123");
		return httpHeaders;
	}
//	@Test
//	public void getPeronByPhoneNumberTest() throws Exception {
//
//		CreatePersonRequest response = dtoUtil.getPrsonRequest();
//
//	}
}
